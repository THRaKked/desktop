package prestamo.desktop.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import prestamo.desktop.view.MenuView;

public class MenuController implements ActionListener{
	private MenuView view;
	
	MenuController(MenuView view){
		
		this.view = view;
		this.view.getMntmSolicitar().addActionListener(this);
		
		
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==view.getMntmSolicitar()) {
			view.setVisible(false);
			
		}
	
	}
	
	

}
