package prestamo.desktop.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import prestamo.desktop.modelo.UserModel;
import prestamo.desktop.view.LoginView;
import prestamo.desktop.view.MenuView;

public class LoginController implements ActionListener{
	LoginView view;
	
	LoginController(LoginView view){
		this.view = view;
		this.view.getBtnIniciarSesion().addActionListener(this);
		this.view.getBtnRegistrate().addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()== view.getBtnIniciarSesion()) {
			String email = view.getEmailField().getText();
			String pass = view.getPasswordField().getText();
//			System.out.println(email+" "+pass);
			UserModel um = new UserModel();
			if(um.autenticate(email, pass)) {
				view.setVisible(false);
				MenuView mv = new MenuView();
				mv.setVisible(true);
				
			}else {
				JOptionPane.showMessageDialog(null, "hay algun preoblema contu usuario o contraseña bro");
			}
			
		}else if(e.getSource()== view.getBtnRegistrate()) {
			
		}
	}
	
	
	

}
