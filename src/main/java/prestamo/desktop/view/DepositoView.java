package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;

public class DepositoView extends JFrame {

	private JPanel contentPane;
	private JTextField txtDeposito;
	private JTextField textField;
	private JTextField txtId;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DepositoView frame = new DepositoView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DepositoView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtDeposito = new JTextField();
		txtDeposito.setBounds(5, 5, 86, 45);
		txtDeposito.setText("Deposito");
		contentPane.add(txtDeposito);
		txtDeposito.setColumns(10);
		
		textField = new JTextField();
		textField.setBounds(111, 17, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		txtId = new JTextField();
		txtId.setText("Id");
		txtId.setBounds(207, 17, 86, 20);
		contentPane.add(txtId);
		txtId.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(319, 17, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnOk = new JButton("Ok");
		btnOk.setBounds(160, 63, 89, 23);
		contentPane.add(btnOk);
	}

}
