package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;

public class MenuView extends JFrame {

	private JPanel contentPane;
	private JMenuItem mntmSolicitar;
	
	/**
	 * Create the frame.
	 */
	public MenuView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 424, 21);
		contentPane.add(menuBar);
		
		JMenuItem mntmInicio = new JMenuItem("Inicio");
		menuBar.add(mntmInicio);
		
		JMenu mnPrestamo = new JMenu("Prestamo");
		menuBar.add(mnPrestamo);
		
		mntmSolicitar = new JMenuItem("Solicitar");
		mnPrestamo.add(mntmSolicitar);
		
		JMenuItem mntmVerificar = new JMenuItem("Verificar");
		mnPrestamo.add(mntmVerificar);
	}

	public JMenuItem getMntmSolicitar() {
		return mntmSolicitar;
	}

	public void setMntmSolicitar(JMenuItem mntmSolicitar) {
		this.mntmSolicitar = mntmSolicitar;
	}
	
	
	
}
