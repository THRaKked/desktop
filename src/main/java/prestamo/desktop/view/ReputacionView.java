package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;

public class ReputacionView extends JFrame {

	private JPanel contentPane;
	private JTextField txtReputacin;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ReputacionView frame = new ReputacionView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ReputacionView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtReputacin = new JTextField();
		txtReputacin.setBounds(5, 96, 86, 61);
		txtReputacin.setText("Reputaci\u00F3n");
		contentPane.add(txtReputacin);
		txtReputacin.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(91, 96, 200, 61);
		contentPane.add(comboBox);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.setBounds(335, 115, 89, 23);
		contentPane.add(btnNewButton);
	}
}
