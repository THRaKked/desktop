package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

public class RegionView extends JFrame {

	private JPanel contentPane;
	private JButton btnSave;
	private JComboBox nombre;
	private JButton btnGrabar;
	
	public JButton getBtnSave() {
		return btnSave;
	}

	public void setBtnSave(JButton btnSave) {
		this.btnSave = btnSave;
	}

	public JComboBox getNombre() {
		return nombre;
	}

	public void setNombre(JComboBox nombre) {
		this.nombre = nombre;
	}

	

	public void setBtnGrabar(JButton btnGrabar) {
		this.btnGrabar = btnGrabar;
	}


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegionView frame = new RegionView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegionView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblRegion = new JLabel("Region");
		lblRegion.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblRegion.setBounds(173, 11, 65, 24);
		contentPane.add(lblRegion);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(25, 73, 46, 14);
		contentPane.add(lblNombre);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"I de Tarapacá", "II de Antofagasta", "III de Atacama", "IV de Coquimbo", "V de Valparaíso", "VI del Libertador General Bernardo O’Higgins", "VII del Maule", "VIII de Bio-Bio", "IX de la Araucanía", "X de Los Lagos", "XI de Aysén del General Carlos Ibañez del Campo", "XII de Magallanes y de la Antártica Chilena", "Metropolitana de Santiago", "XIV de Los Rios", "XVI del Ñuble"}));
		comboBox.setBounds(135, 70, 210, 20);
		contentPane.add(comboBox);
		
		JButton btnSave = new JButton("Save");
		btnSave.setBounds(256, 166, 89, 23);
		contentPane.add(btnSave);
	}

}
