package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.JTextField;

public class usuarioView extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					usuarioView frame = new usuarioView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public usuarioView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsuario = new JLabel("USUARIO");
		lblUsuario.setBounds(170, 8, 129, 24);
		contentPane.add(lblUsuario);
		
		JButton btnSave = new JButton("Save");
		btnSave.setBounds(314, 227, 89, 23);
		contentPane.add(btnSave);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 57, 46, 14);
		contentPane.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(10, 82, 46, 14);
		contentPane.add(lblApellido);
		
		JLabel lblCorreo = new JLabel("Correo");
		lblCorreo.setBounds(10, 107, 46, 14);
		contentPane.add(lblCorreo);
		
		JLabel lblTipo = new JLabel("tipo");
		lblTipo.setBounds(10, 132, 46, 14);
		contentPane.add(lblTipo);
		
		JLabel lblCalle = new JLabel("Calle");
		lblCalle.setBounds(10, 157, 46, 14);
		contentPane.add(lblCalle);
		
		JLabel lblNroDeCasa = new JLabel("nro de casa");
		lblNroDeCasa.setBounds(10, 182, 56, 14);
		contentPane.add(lblNroDeCasa);
		
		JLabel lblRut = new JLabel("Rut");
		lblRut.setBounds(10, 32, 46, 14);
		contentPane.add(lblRut);
		
		textField = new JTextField();
		textField.setBounds(61, 29, 144, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(66, 54, 129, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(66, 79, 129, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(65, 104, 258, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(66, 129, 86, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		textField_5 = new JTextField();
		textField_5.setBounds(66, 157, 258, 20);
		contentPane.add(textField_5);
		textField_5.setColumns(10);
		
		textField_6 = new JTextField();
		textField_6.setBounds(66, 179, 86, 20);
		contentPane.add(textField_6);
		textField_6.setColumns(10);
		
		JLabel lblReputacinid = new JLabel("reputación_id");
		lblReputacinid.setBounds(10, 207, 65, 14);
		contentPane.add(lblReputacinid);
		
		textField_7 = new JTextField();
		textField_7.setBounds(85, 204, 120, 20);
		contentPane.add(textField_7);
		textField_7.setColumns(10);
		
		JLabel lblComunaid = new JLabel("comuna_id");
		lblComunaid.setBounds(10, 231, 56, 14);
		contentPane.add(lblComunaid);
		
		textField_8 = new JTextField();
		textField_8.setBounds(66, 228, 120, 20);
		contentPane.add(textField_8);
		textField_8.setColumns(10);
	}
}
