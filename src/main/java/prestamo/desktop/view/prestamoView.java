package prestamo.desktop.view;



import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class prestamoView extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;


	/**
	 * Create the frame.
	 */
	public prestamoView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPrestamo = new JLabel("Prestamo");
		lblPrestamo.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblPrestamo.setBounds(171, 11, 74, 14);
		contentPane.add(lblPrestamo);
		
		JLabel lblFecha = new JLabel("Fecha");
		lblFecha.setBounds(10, 69, 49, 28);
		contentPane.add(lblFecha);
		
		JLabel lblDia = new JLabel("dia");
		lblDia.setBounds(69, 48, 25, 14);
		contentPane.add(lblDia);
		
		JLabel lblMes = new JLabel("mes");
		lblMes.setBounds(152, 48, 25, 14);
		contentPane.add(lblMes);
		
		JLabel lblAo = new JLabel("año");
		lblAo.setBounds(237, 48, 32, 14);
		contentPane.add(lblAo);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"}));
		comboBox.setBounds(57, 73, 55, 20);
		contentPane.add(comboBox);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"}));
		comboBox_1.setBounds(143, 73, 55, 20);
		contentPane.add(comboBox_1);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"", "2018", "2019"}));
		comboBox_2.setBounds(227, 73, 42, 20);
		contentPane.add(comboBox_2);
		
		JLabel lblMotivo = new JLabel("motivo");
		lblMotivo.setBounds(10, 108, 46, 14);
		contentPane.add(lblMotivo);
		
		textField = new JTextField();
		textField.setBounds(80, 104, 118, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblMonto = new JLabel("monto");
		lblMonto.setBounds(13, 150, 46, 14);
		contentPane.add(lblMonto);
		
		textField_1 = new JTextField();
		textField_1.setBounds(80, 147, 118, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblCuotas = new JLabel("cuotas");
		lblCuotas.setBounds(10, 185, 46, 14);
		contentPane.add(lblCuotas);
		
		textField_2 = new JTextField();
		textField_2.setBounds(80, 182, 118, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblUsuarioid = new JLabel("usuario_id");
		lblUsuarioid.setBounds(10, 223, 49, 14);
		contentPane.add(lblUsuarioid);
		
		textField_3 = new JTextField();
		textField_3.setBounds(80, 220, 118, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JButton btnSave = new JButton("Save");
		btnSave.setBounds(309, 219, 89, 23);
		contentPane.add(btnSave);
	}
}
