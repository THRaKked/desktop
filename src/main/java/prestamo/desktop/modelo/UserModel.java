package prestamo.desktop.modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserModel implements Model{
	
	private String id;
	private String rut;
	private String nombre;
	private String apellido;
	private String correo;
	private String tipo;
	private String calle;
	private String numeroCasa;
	private String comunaId;
	private String regionId;
	private String reputacionId;
	@Override
	public boolean create() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean update() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean delete() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean get() {
		// TODO Auto-generated method stub
		return false;
	}
	
	public boolean autenticate(String email, String pass) {
		boolean resultado = false;
		String sql = "SELECT * FROM Usuario WHERE correo = ? AND clave = ?";
		Connection con = DB.getConnection();
		
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, email);
			ps.setString(2, pass);
			ResultSet rs = ps.executeQuery();
			resultado = rs.next();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultado;
	}
	

}

